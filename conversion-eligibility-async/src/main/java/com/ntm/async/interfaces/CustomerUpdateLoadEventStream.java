package com.ntm.async.interfaces;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface CustomerUpdateLoadEventStream {
  String INPUT = "CustomerUpdateLoadEvent_In";

  @Input(INPUT)
  SubscribableChannel customerUpdateLoadEventChannel();

}
