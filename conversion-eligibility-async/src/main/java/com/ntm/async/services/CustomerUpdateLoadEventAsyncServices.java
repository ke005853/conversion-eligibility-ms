package com.ntm.async.services;

import javax.inject.Provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.ntm.async.interfaces.CustomerUpdateLoadEventStream;
import com.ntm.async.mappers.interfaces.IMapReleaseEventToReleaseEventDTO;
import com.ntm.async.model.ReleaseEvent;
import com.ntm.persistence.entities.ReleaseEventDTO;
import com.ntm.persistence.services.interfaces.CustomerUpdateLoadEventRepoService;

@Component
public class CustomerUpdateLoadEventAsyncServices {


  @Autowired
  private CustomerUpdateLoadEventRepoService customerUpdateLoadEventRepoService;

  @Autowired
  private Provider<IMapReleaseEventToReleaseEventDTO> mapReleaseEventToReleaseEventDTOProvider;

  @StreamListener(CustomerUpdateLoadEventStream.INPUT)
  public void loadEvent(@Payload ReleaseEvent event) {
    System.out.println("Event Processed: " + event.toString());
    // Map Event to DTO

    IMapReleaseEventToReleaseEventDTO mapReleaseEventToReleaseEventDTO =
        mapReleaseEventToReleaseEventDTOProvider.get();
    ReleaseEventDTO releaseEventDTO =
        mapReleaseEventToReleaseEventDTO.mapReleaseEventToReleaseEventDTO(event);

    customerUpdateLoadEventRepoService.saveReleaseEvent(releaseEventDTO);

  }
}
