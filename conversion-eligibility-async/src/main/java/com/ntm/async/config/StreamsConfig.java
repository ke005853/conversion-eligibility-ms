package com.ntm.async.config;

import org.springframework.cloud.stream.annotation.EnableBinding;

import com.ntm.async.interfaces.CustomerUpdateLoadEventStream;

@EnableBinding(CustomerUpdateLoadEventStream.class)
public class StreamsConfig {

}
