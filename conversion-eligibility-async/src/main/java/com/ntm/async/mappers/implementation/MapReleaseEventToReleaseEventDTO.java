package com.ntm.async.mappers.implementation;

import org.springframework.stereotype.Component;

import com.ntm.async.mappers.interfaces.IMapReleaseEventToReleaseEventDTO;
import com.ntm.async.model.ReleaseEvent;
import com.ntm.persistence.entities.ReleaseEventDTO;

@Component
public class MapReleaseEventToReleaseEventDTO implements IMapReleaseEventToReleaseEventDTO {


  public ReleaseEventDTO mapReleaseEventToReleaseEventDTO(ReleaseEvent releaseEvent) {
    if (releaseEvent == null) {
      return null;
    }
    ReleaseEventDTO releaseEventDTO = new ReleaseEventDTO();

    releaseEventDTO.setBanId(releaseEvent.getBanId());
    releaseEventDTO.setApplicationId(releaseEvent.getApplicationId());
    releaseEventDTO.setBanConsumedTime(System.nanoTime());

    return releaseEventDTO;
  }
}
