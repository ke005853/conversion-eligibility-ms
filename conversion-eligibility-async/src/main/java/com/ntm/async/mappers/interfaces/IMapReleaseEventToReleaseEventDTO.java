package com.ntm.async.mappers.interfaces;

import com.ntm.async.model.ReleaseEvent;
import com.ntm.persistence.entities.ReleaseEventDTO;

public interface IMapReleaseEventToReleaseEventDTO {

  public ReleaseEventDTO mapReleaseEventToReleaseEventDTO(ReleaseEvent releaseEvent);
}
