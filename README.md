# MSTemplate
Template for Microservice

## Usage

### Generate the Parent Project
```maven
mvn archetype:generate -DgroupId=com.ntm -DartifactId=conversion-eligibility-ms
cd conversion-eligibility-ms
```

### Generate the Necessary child modules:
#### conversion-eligibility-core - Contains the Main class - Spring boot
```maven
mvn archetype:generate -DgroupId=com.ntm -DartifactId=conversion-eligibility-core
```
#### conversion-eligibility-api -  Contains the swagger api in OpenAPI specifications. Generates the Classes and Delegate Intefaces.
```maven
mvn archetype:generate -DgroupId=com.ntm -DartifactId=conversion-eligibility-api
```
##### Copy the swagger to the src/main/resources/swagger location. And based on the plugin it will generate the necessary classes and Delegates. 
#### conversion-eligibility-delegate - Contains the Implementation of Module conversion-eligibility-api's Delegates.
```maven
mvn archetype:generate -DgroupId=com.ntm -DartifactId=conversion-eligibility-delegate
```
#### conversion-eligibility-persistence - Contains the Implementation for Persistence layer.
```maven
mvn archetype:generate -DgroupId=com.ntm -DartifactId=conversion-eligibility-persistence
```
#### conversion-eligibility-asyncapi
```maven
mvn archetype:generate -DgroupId=com.ntm -DartifactId=conversion-eligibility-asyncapi
```
#### conversion-eligibility-async
```maven
mvn archetype:generate -DgroupId=com.ntm -DartifactId=conversion-eligibility-async
```
#### conversion-eligibility-extapi
```maven
mvn archetype:generate -DgroupId=com.ntm -DartifactId=conversion-eligibility-extapi
```
#### conversion-eligibility-extservice
```maven
mvn archetype:generate -DgroupId=com.ntm -DartifactId=conversion-eligibility-extservice
```