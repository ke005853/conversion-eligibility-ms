/**
 * 
 */
package com.ntm.delegate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ntm.api.api.ConversionEligibleApiDelegate;
import com.ntm.api.model.ConversionEligibility;
import com.ntm.api.model.ConversionEligibilityResponse;
import com.ntm.delegate.mappers.interfaces.IMapListConversionEligibleBansToConversionEligibilityResponse;
import com.ntm.persistence.entities.ConversionEligibilityDTO;
import com.ntm.persistence.entities.ConversionEligibilityDataDTO;
import com.ntm.persistence.services.impl.ConversionEligibilityRepoServiceImpl;

/**
 * @author ke005853
 *
 */
@Service
public class ConversionEligibleApiDelegateImpl implements ConversionEligibleApiDelegate {

	@Autowired
	private ConversionEligibilityRepoServiceImpl conversionEligibilityRepoServiceImpl;

	@Autowired
	private IMapListConversionEligibleBansToConversionEligibilityResponse mapResponse;

	@Override
	public ResponseEntity<ConversionEligibilityResponse> conversionEligibility(ConversionEligibility body,
			String acceptLanguage, String applicationID, String applicationUser) {

		
		ConversionEligibilityDataDTO conversionEligibilityDataDTO = new ConversionEligibilityDataDTO();
		conversionEligibilityDataDTO.setFromDate(body.getConversionEligibilityData().getFromDate());
		conversionEligibilityDataDTO.setToDate(body.getConversionEligibilityData().getToDate());

		ConversionEligibilityDTO conversionEligibilityDTO = new ConversionEligibilityDTO();
		conversionEligibilityDTO.setConversionEligibilityData(conversionEligibilityDataDTO);

		ConversionEligibilityResponse response = mapResponse.map(conversionEligibilityRepoServiceImpl.findAllUsingN1ql(
				body.getConversionEligibilityData().getFromDate().toString(),
				body.getConversionEligibilityData().getToDate().toString()));
		
		return ResponseEntity.ok(response);
	}
}
