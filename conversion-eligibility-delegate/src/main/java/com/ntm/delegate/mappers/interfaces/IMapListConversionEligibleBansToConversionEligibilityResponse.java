package com.ntm.delegate.mappers.interfaces;

import java.util.List;

import com.ntm.api.model.ConversionEligibilityResponse;
import com.ntm.persistence.entities.ConversionEligibleBans;

public interface IMapListConversionEligibleBansToConversionEligibilityResponse {

	public ConversionEligibilityResponse map(List<ConversionEligibleBans> conversionEligibilityBans);
}
