package com.ntm.delegate.mappers.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.ntm.api.model.ConversionEliDetailsInfo;
import com.ntm.api.model.ConversionEligibilityResponse;
import com.ntm.api.model.ConversionEligibilityResponseData;
import com.ntm.delegate.mappers.interfaces.IMapListConversionEligibleBansToConversionEligibilityResponse;
import com.ntm.persistence.entities.ConversionEligibleBans;

@Service
public class MapListConversionEligibleBansToConversionEligibilityResponse
		implements IMapListConversionEligibleBansToConversionEligibilityResponse {

	@Override
	public ConversionEligibilityResponse map(List<ConversionEligibleBans> conversionEligibilityBans) {
		if (conversionEligibilityBans == null) {
			return null;
		}

		ConversionEligibilityResponse response = new ConversionEligibilityResponse();
		ConversionEligibilityResponseData conversionEligibilityResponseData = new ConversionEligibilityResponseData();

		List<ConversionEliDetailsInfo> list = new ArrayList<>();
		
		for (ConversionEligibleBans conversionEligibleBans : conversionEligibilityBans) {
			ConversionEliDetailsInfo conversionEliDetails = new ConversionEliDetailsInfo();
			conversionEliDetails.setCount(conversionEligibleBans.getCount());
			conversionEliDetails.setDate(conversionEligibleBans.getDate());
			conversionEliDetails.setTime(conversionEligibleBans.getTime());
			conversionEliDetails.setTimeInNS(conversionEligibleBans.getTimeInNS());
			list.add(conversionEliDetails);
		}

		conversionEligibilityResponseData.setConversionEliDetails(list);
		response.setConversionEligibilityResponseData(conversionEligibilityResponseData);

		return response;
	}

}
