package com.ntm.extservice.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.ntm.api.api.PetApi;
import com.ntm.api.model.Pet;
import com.ntm.extservice.service.interfaces.ICallService;

@Component
public class CallServiceImpl implements ICallService {

	@Autowired
	@Qualifier(value = "petApiCustom")
	private PetApi petapi;

	@Override
	public void callService() {
		Pet pet = petapi.getPetById(2L);
		System.out.println("PET : "+pet.getName());
	}

}
