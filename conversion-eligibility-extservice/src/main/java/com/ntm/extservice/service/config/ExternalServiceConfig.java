package com.ntm.extservice.service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ntm.api.ApiClient;
import com.ntm.api.api.PetApi;

@Configuration
public class ExternalServiceConfig {
	private static final String BASE_PATH = "http://petstore.swagger.io/v2";

	@Bean(value = "petApiCustom")
	public PetApi petApi() {
		return new PetApi(apiClient());
	}

	/**
	 * The ApiClient class is used for configuring authentication, the base path of
	 * the API, common headers, and it�s responsible for executing all API
	 * requests.<br>
	 */
	@Bean
	public ApiClient apiClient() {
		ApiClient apiClient = new ApiClient();
		// Endpoint basePath
		apiClient.setBasePath(BASE_PATH);
		return apiClient;
	}
}
