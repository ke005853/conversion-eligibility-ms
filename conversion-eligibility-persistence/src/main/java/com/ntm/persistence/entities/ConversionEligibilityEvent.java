package com.ntm.persistence.entities;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;
import org.springframework.data.couchbase.core.mapping.id.GeneratedValue;
import org.springframework.data.couchbase.core.mapping.id.GenerationStrategy;


@Document
public class ConversionEligibilityEvent implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @NotNull
  @Id
  @GeneratedValue(strategy = GenerationStrategy.UNIQUE)
  private String id;

  @NotNull
  @Field
  private String banId;

  @Field
  private String applicationId;

  @Field
  private String banUpdateTime;

  @Field
  private long banConsumedTime;

  @Field
  private long banProcessingTime;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getBanId() {
    return banId;
  }

  public void setBanId(String banId) {
    this.banId = banId;
  }

  public String getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(String applicationId) {
    this.applicationId = applicationId;
  }

  public String getBanUpdateTime() {
    return banUpdateTime;
  }

  public void setBanUpdateTime(String banUpdateTime) {
    this.banUpdateTime = banUpdateTime;
  }

  public long getBanConsumedTime() {
    return banConsumedTime;
  }

  public void setBanConsumedTime(long banConsumedTime) {
    this.banConsumedTime = banConsumedTime;
  }

  public long getBanProcessingTime() {
    return banProcessingTime;
  }

  public void setBanProcessingTime(long banProcessingTime) {
    this.banProcessingTime = banProcessingTime;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }


}
