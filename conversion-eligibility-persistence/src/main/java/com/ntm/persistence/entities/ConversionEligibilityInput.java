package com.ntm.persistence.entities;

import java.io.Serializable;

import org.springframework.data.couchbase.core.mapping.Document;


@Document
public class ConversionEligibilityInput implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 4597784713668354101L;

  private ConversionEligibilityData conversionEligibilityData;

  public ConversionEligibilityInput() {}

  public ConversionEligibilityInput(ConversionEligibilityData conversionEligibilityData) {
    super();
    this.conversionEligibilityData = conversionEligibilityData;
  }

  public ConversionEligibilityData getConversionEligibilityData() {
    return conversionEligibilityData;
  }

  public void setConversionEligibilityData(ConversionEligibilityData conversionEligibilityData) {
    this.conversionEligibilityData = conversionEligibilityData;
  }



}
