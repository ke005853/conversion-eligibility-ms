package com.ntm.persistence.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;

@Document
public class ConversionEligibleBans implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 5088594840283225294L;

  @NotNull
  @Id
  private String id;

  @Field
  private LocalDate date;

  @Field
  private BigDecimal time;

  @Field
  private BigDecimal count;

  @Field
  private BigDecimal timeInNS;

  public java.lang.String getId() {
    return id;
  }

  public void setId(java.lang.String id) {
    this.id = id;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public BigDecimal getTime() {
    return time;
  }

  public void setTime(BigDecimal time) {
    this.time = time;
  }

  public BigDecimal getCount() {
    return count;
  }

  public void setCount(BigDecimal count) {
    this.count = count;
  }

  public BigDecimal getTimeInNS() {
    return this.timeInNS;
  }

  public void setTimeInNS(BigDecimal timeInNS) {
    this.timeInNS = timeInNS;
  }

  public ConversionEligibleBans() {
    // TODO Auto-generated constructor stub
  }

  public ConversionEligibleBans(String id, LocalDate date, BigDecimal time, BigDecimal count) {
    super();
    this.id = id;
    this.date = date;
    this.time = time;
    this.count = count;
  }


}
