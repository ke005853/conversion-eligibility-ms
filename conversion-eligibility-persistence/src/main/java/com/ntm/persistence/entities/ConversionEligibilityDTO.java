package com.ntm.persistence.entities;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.core.deps.com.fasterxml.jackson.databind.annotation.JsonDeserialize;


@Document
public class ConversionEligibilityDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  @NotNull
  @Id
  private String id;

  @JsonDeserialize(contentAs = ConversionEligibilityDataDTO.class)
  private ConversionEligibilityDataDTO conversionEligibilityDataDTO;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public ConversionEligibilityDataDTO getConversionEligibilityData() {
    return conversionEligibilityDataDTO;
  }

  public void setConversionEligibilityData(
      ConversionEligibilityDataDTO conversionEligibilityDataDTO) {
    this.conversionEligibilityDataDTO = conversionEligibilityDataDTO;
  }

}

