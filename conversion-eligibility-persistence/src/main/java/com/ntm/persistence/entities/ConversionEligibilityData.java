package com.ntm.persistence.entities;

import java.io.Serializable;
import java.time.LocalDate;

import org.springframework.data.couchbase.core.mapping.Document;


@Document
public class ConversionEligibilityData implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private LocalDate fromDate;
  private LocalDate toDate;

  public ConversionEligibilityData() {}

  public ConversionEligibilityData(LocalDate fromDate, LocalDate toDate) {
    super();
    this.fromDate = fromDate;
    this.toDate = toDate;
  }

  public LocalDate getFromDate() {
    return fromDate;
  }

  public void setFromDate(LocalDate fromDate) {
    this.fromDate = fromDate;
  }

  public LocalDate getToDate() {
    return toDate;
  }

  public void setToDate(LocalDate toDate) {
    this.toDate = toDate;
  }
}
