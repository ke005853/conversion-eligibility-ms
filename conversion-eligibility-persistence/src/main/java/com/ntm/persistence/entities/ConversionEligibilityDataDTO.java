package com.ntm.persistence.entities;

import java.time.LocalDate;

import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;


@Document
public class ConversionEligibilityDataDTO {

  @Field
  private LocalDate fromDate;

  @Field
  private LocalDate toDate;

  public LocalDate getFromDate() {
    return fromDate;
  }

  public void setFromDate(LocalDate fromDate) {
    this.fromDate = fromDate;
  }

  public LocalDate getToDate() {
    return toDate;
  }

  public void setToDate(LocalDate toDate) {
    this.toDate = toDate;
  }
}

