package com.ntm.persistence.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

@Configuration
@EnableCouchbaseRepositories(basePackages = {"com.ntm.persistence"})
public class RepositoryConfig extends AbstractCouchbaseConfiguration{

	@Value("${spring.couchbase.connection-string}")
	private String couchbaseConnection; 
	
	@Value("${spring.couchbase.username}")
	private String userName; 
	
	@Value("${spring.couchbase.password}")
	private String password; 
	
	@Value("${spring.data.couchbase.bucket-name}")
	private String bucketName;
	
	
	@Override
	public String getConnectionString() {
		return "couchbase://"+this.couchbaseConnection;
	}

	@Override
	public String getUserName() {
		return this.userName;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public String getBucketName() {
		return this.bucketName;
	}

}
