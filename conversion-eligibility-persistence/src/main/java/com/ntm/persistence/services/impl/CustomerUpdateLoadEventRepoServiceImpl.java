package com.ntm.persistence.services.impl;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ntm.persistence.entities.ReleaseEventDTO;
import com.ntm.persistence.repositories.CustomerUpdateLoadEventRepository;
import com.ntm.persistence.services.interfaces.CustomerUpdateLoadEventRepoService;

@Service
public class CustomerUpdateLoadEventRepoServiceImpl implements CustomerUpdateLoadEventRepoService {

  @Autowired
  private CustomerUpdateLoadEventRepository customerUpdateLoadEventRepository;

  public void saveReleaseEvent(ReleaseEventDTO event) {
    event.setId(event.getBanId() + "::" + event.getApplicationId() + "::" + UUID.randomUUID());
    event.setBanProcessingTime(System.nanoTime() - event.getBanConsumedTime());
    event.setBanUpdateTime(OffsetDateTime.now().toString());
    customerUpdateLoadEventRepository.save(event);
  }

}
