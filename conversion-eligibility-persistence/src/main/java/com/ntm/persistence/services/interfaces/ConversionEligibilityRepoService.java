package com.ntm.persistence.services.interfaces;

import java.util.List;

import com.ntm.persistence.entities.ConversionEligibilityDTO;
import com.ntm.persistence.entities.ConversionEligibleBans;

public interface ConversionEligibilityRepoService {
  public void saveConversionEligibility(ConversionEligibilityDTO conversionEligibilityDTO);
  public List<ConversionEligibleBans> findAllUsingN1ql(String fromDate, String toDate);
}
