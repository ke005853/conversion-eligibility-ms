package com.ntm.persistence.services.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ntm.persistence.entities.ConversionEligibilityDTO;
import com.ntm.persistence.entities.ConversionEligibleBans;
import com.ntm.persistence.repositories.ConversionEligibilityRepository;
import com.ntm.persistence.services.interfaces.ConversionEligibilityRepoService;

@Service
public class ConversionEligibilityRepoServiceImpl implements ConversionEligibilityRepoService {

	@Autowired
	private ConversionEligibilityRepository conversionEligibilityRepository;
	
	/*
	 * @Autowired private CouchbaseTemplate couchbaseTemplate;
	 * 
	 * @Autowired private Cluster cluster;
	 */
    
	public void saveConversionEligibility(ConversionEligibilityDTO conversionEligibilityDTO) {
		conversionEligibilityDTO.setId(UUID.randomUUID().toString());
		conversionEligibilityRepository.save(conversionEligibilityDTO);
	}

	public List<ConversionEligibleBans> findAllUsingN1ql(String fromDate, String toDate) {
		return (List<ConversionEligibleBans>) conversionEligibilityRepository.queyByN1ql(fromDate, toDate);
	}

}
