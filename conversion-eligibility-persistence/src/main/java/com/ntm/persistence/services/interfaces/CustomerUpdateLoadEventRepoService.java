package com.ntm.persistence.services.interfaces;

import com.ntm.persistence.entities.ReleaseEventDTO;

public interface CustomerUpdateLoadEventRepoService {
  public void saveReleaseEvent(ReleaseEventDTO event);
}
