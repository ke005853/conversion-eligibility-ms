package com.ntm.persistence.repositories;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.ntm", "com.ntm.persistance.async"})
public class RepositoriesConfig {

}
