package com.ntm.persistence.repositories;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.data.couchbase.repository.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import com.ntm.persistence.entities.ConversionEligibilityDTO;
import com.ntm.persistence.entities.ConversionEligibleBans;

@Repository
public interface ConversionEligibilityRepository extends CouchbaseRepository<ConversionEligibilityDTO, String> {

	@Async
	@Query("select a.count as count, a.date as date,a.time as time, a.timeInNS as timeInNS from (SELECT count(1) as count,DATE_FORMAT_STR(banUpdateTime,'1111-11-11') as date, \r\n"
			+ "DATE_PART_STR(banUpdateTime,'hour') as time,avg(processingTime) as timeInNS, 'com.sprint.parent.entity.ConversionEligibilityEvent' as _class \r\n"
			+ "FROM `com.amdocs.digital.ms.conversion.eligibility`\r\n" + "WHERE banUpdateTime IS NOT NULL\r\n"
			+ "and banUpdateTime between DATE_FORMAT_STR($fromDate,'1111-11-11') and DATE_FORMAT_STR(DATE_ADD_STR($toDate, 1, 'day'),'1111-11-11')\r\n"
			+ "GROUP BY DATE_FORMAT_STR(banUpdateTime,'1111-11-11'),DATE_PART_STR(banUpdateTime,'hour'), _class\r\n"
			+ "ORDER BY DATE_FORMAT_STR(banUpdateTime,'1111-11-11'),DATE_PART_STR(banUpdateTime,'hour')) a \r\n"
			+ " where a.date is not null \r\n"
			+ " and _class = 'com.sprint.parent.entity.ConversionEligibilityEvent'")
	CompletableFuture<List<ConversionEligibleBans>> queyByN1ql(String fromDate, String toDate);
}
