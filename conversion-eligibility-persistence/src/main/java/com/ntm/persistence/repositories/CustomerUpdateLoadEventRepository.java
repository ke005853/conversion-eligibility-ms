package com.ntm.persistence.repositories;

import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.ntm.persistence.entities.ReleaseEventDTO;

@Repository
public interface CustomerUpdateLoadEventRepository
    extends CouchbaseRepository<ReleaseEventDTO, String> {

}
