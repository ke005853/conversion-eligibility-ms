package com.ntm.api.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}