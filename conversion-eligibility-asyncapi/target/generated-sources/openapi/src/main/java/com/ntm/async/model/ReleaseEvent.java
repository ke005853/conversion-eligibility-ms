package com.ntm.async.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ReleaseEvent
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-07-09T10:22:55.002-05:00[America/Chicago]")

public class ReleaseEvent   {
  @JsonProperty("banId")
  private String banId;

  @JsonProperty("applicationId")
  private String applicationId;

  public ReleaseEvent banId(String banId) {
    this.banId = banId;
    return this;
  }

  /**
   * Get banId
   * @return banId
  */
  @ApiModelProperty(value = "")


  public String getBanId() {
    return banId;
  }

  public void setBanId(String banId) {
    this.banId = banId;
  }

  public ReleaseEvent applicationId(String applicationId) {
    this.applicationId = applicationId;
    return this;
  }

  /**
   * Get applicationId
   * @return applicationId
  */
  @ApiModelProperty(value = "")


  public String getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(String applicationId) {
    this.applicationId = applicationId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ReleaseEvent releaseEvent = (ReleaseEvent) o;
    return Objects.equals(this.banId, releaseEvent.banId) &&
        Objects.equals(this.applicationId, releaseEvent.applicationId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(banId, applicationId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ReleaseEvent {\n");
    
    sb.append("    banId: ").append(toIndentedString(banId)).append("\n");
    sb.append("    applicationId: ").append(toIndentedString(applicationId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

