package com.ntm.api.api;

import com.ntm.api.model.ConversionEligibility;
import com.ntm.api.model.ConversionEligibilityResponse;
import com.ntm.api.model.ErrorResponse;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A delegate to be called by the {@link ConversionEligibleApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-07-09T10:40:26.609-05:00[America/Chicago]")

public interface ConversionEligibleApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /conversionEligible
     * This service retrieves the number of Eligible customer. 
     *
     * @param body Eligibility details needs to be retrieved (required)
     * @param acceptLanguage client&#39;s locale  (optional)
     * @param applicationID Application Id (optional)
     * @param applicationUser Application User (optional)
     * @return *OK* - The Eligibility Information was Retrieved successfully.  (status code 200)
     *         or *Bad Request* - The request was invalid.  (status code 400)
     *         or *Unauthorized* - Authorization failed.  (status code 401)
     *         or *Not Found* - The server is down or cannot be reached.  (status code 404)
     *         or *Method Not Allowed* -  The server has rejected the specified HTTP request method.  (status code 405)
     *         or *Internal Server Error* - The server could not fulfill the request.  (status code 500)
     * @see ConversionEligibleApi#conversionEligibility
     */
    default ResponseEntity<ConversionEligibilityResponse> conversionEligibility(ConversionEligibility body,
        String acceptLanguage,
        String applicationID,
        String applicationUser) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"conversionEligibilityResponseData\" : { \"fromDate\" : \"2000-01-23\", \"conversionEliDetails\" : [ { \"date\" : \"2000-01-23\", \"count\" : 6.027456183070403, \"time\" : 0.8008281904610115, \"timeInNS\" : 1.4658129805029452 }, { \"date\" : \"2000-01-23\", \"count\" : 6.027456183070403, \"time\" : 0.8008281904610115, \"timeInNS\" : 1.4658129805029452 } ], \"toDate\" : \"2000-01-23\" } }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
