package com.ntm.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ntm.api.model.ConversionEligibilityData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Conversion Eligibility Request data.
 */
@ApiModel(description = "Conversion Eligibility Request data.")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-07-09T10:40:26.609-05:00[America/Chicago]")

public class ConversionEligibility   {
  @JsonProperty("conversionEligibilityData")
  private ConversionEligibilityData conversionEligibilityData;

  public ConversionEligibility conversionEligibilityData(ConversionEligibilityData conversionEligibilityData) {
    this.conversionEligibilityData = conversionEligibilityData;
    return this;
  }

  /**
   * Get conversionEligibilityData
   * @return conversionEligibilityData
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ConversionEligibilityData getConversionEligibilityData() {
    return conversionEligibilityData;
  }

  public void setConversionEligibilityData(ConversionEligibilityData conversionEligibilityData) {
    this.conversionEligibilityData = conversionEligibilityData;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConversionEligibility conversionEligibility = (ConversionEligibility) o;
    return Objects.equals(this.conversionEligibilityData, conversionEligibility.conversionEligibilityData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(conversionEligibilityData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConversionEligibility {\n");
    
    sb.append("    conversionEligibilityData: ").append(toIndentedString(conversionEligibilityData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

