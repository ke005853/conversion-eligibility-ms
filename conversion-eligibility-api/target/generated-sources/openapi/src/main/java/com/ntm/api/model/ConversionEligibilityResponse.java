package com.ntm.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ntm.api.model.ConversionEligibilityResponseData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Conversion Eligibility Request data
 */
@ApiModel(description = "Conversion Eligibility Request data")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-07-09T10:40:26.609-05:00[America/Chicago]")

public class ConversionEligibilityResponse   {
  @JsonProperty("conversionEligibilityResponseData")
  private ConversionEligibilityResponseData conversionEligibilityResponseData;

  public ConversionEligibilityResponse conversionEligibilityResponseData(ConversionEligibilityResponseData conversionEligibilityResponseData) {
    this.conversionEligibilityResponseData = conversionEligibilityResponseData;
    return this;
  }

  /**
   * Get conversionEligibilityResponseData
   * @return conversionEligibilityResponseData
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ConversionEligibilityResponseData getConversionEligibilityResponseData() {
    return conversionEligibilityResponseData;
  }

  public void setConversionEligibilityResponseData(ConversionEligibilityResponseData conversionEligibilityResponseData) {
    this.conversionEligibilityResponseData = conversionEligibilityResponseData;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConversionEligibilityResponse conversionEligibilityResponse = (ConversionEligibilityResponse) o;
    return Objects.equals(this.conversionEligibilityResponseData, conversionEligibilityResponse.conversionEligibilityResponseData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(conversionEligibilityResponseData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConversionEligibilityResponse {\n");
    
    sb.append("    conversionEligibilityResponseData: ").append(toIndentedString(conversionEligibilityResponseData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

