package com.ntm.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * The response containing the descriptive error text and error code
 */
@ApiModel(description = "The response containing the descriptive error text and error code")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-07-09T10:40:26.609-05:00[America/Chicago]")

public class ErrorResponse   {
  @JsonProperty("link")
  private String link;

  @JsonProperty("rel")
  private String rel = "help";

  @JsonProperty("code")
  private String code;

  @JsonProperty("message")
  private String message;

  @JsonProperty("traceId")
  private String traceId;

  public ErrorResponse link(String link) {
    this.link = link;
    return this;
  }

  /**
   * The self-reference to the item 
   * @return link
  */
  @ApiModelProperty(value = "The self-reference to the item ")


  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public ErrorResponse rel(String rel) {
    this.rel = rel;
    return this;
  }

  /**
   * The relation type of the item 
   * @return rel
  */
  @ApiModelProperty(value = "The relation type of the item ")


  public String getRel() {
    return rel;
  }

  public void setRel(String rel) {
    this.rel = rel;
  }

  public ErrorResponse code(String code) {
    this.code = code;
    return this;
  }

  /**
   * The code associated with the error 
   * @return code
  */
  @ApiModelProperty(example = "404 or 500 or 401 ", value = "The code associated with the error ")


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public ErrorResponse message(String message) {
    this.message = message;
    return this;
  }

  /**
   * The message associated with the error 
   * @return message
  */
  @ApiModelProperty(example = "Not Found - Server unable to locate the requested URI", value = "The message associated with the error ")


  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public ErrorResponse traceId(String traceId) {
    this.traceId = traceId;
    return this;
  }

  /**
   * The unique trace ID used to correlate the error returned to a client with the server 
   * @return traceId
  */
  @ApiModelProperty(example = "123", value = "The unique trace ID used to correlate the error returned to a client with the server ")


  public String getTraceId() {
    return traceId;
  }

  public void setTraceId(String traceId) {
    this.traceId = traceId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ErrorResponse errorResponse = (ErrorResponse) o;
    return Objects.equals(this.link, errorResponse.link) &&
        Objects.equals(this.rel, errorResponse.rel) &&
        Objects.equals(this.code, errorResponse.code) &&
        Objects.equals(this.message, errorResponse.message) &&
        Objects.equals(this.traceId, errorResponse.traceId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(link, rel, code, message, traceId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ErrorResponse {\n");
    
    sb.append("    link: ").append(toIndentedString(link)).append("\n");
    sb.append("    rel: ").append(toIndentedString(rel)).append("\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    traceId: ").append(toIndentedString(traceId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

