package com.ntm.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * The details of the Request data  
 */
@ApiModel(description = "The details of the Request data  ")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-07-09T10:40:26.609-05:00[America/Chicago]")

public class ConversionEligibilityData   {
  @JsonProperty("fromDate")
  private LocalDate fromDate;

  @JsonProperty("toDate")
  private LocalDate toDate;

  @JsonProperty("newField")
  private String newField;

  public ConversionEligibilityData fromDate(LocalDate fromDate) {
    this.fromDate = fromDate;
    return this;
  }

  /**
   * Creation date
   * @return fromDate
  */
  @ApiModelProperty(required = true, value = "Creation date")
  @NotNull

  @Valid

  public LocalDate getFromDate() {
    return fromDate;
  }

  public void setFromDate(LocalDate fromDate) {
    this.fromDate = fromDate;
  }

  public ConversionEligibilityData toDate(LocalDate toDate) {
    this.toDate = toDate;
    return this;
  }

  /**
   * updated date
   * @return toDate
  */
  @ApiModelProperty(required = true, value = "updated date")
  @NotNull

  @Valid

  public LocalDate getToDate() {
    return toDate;
  }

  public void setToDate(LocalDate toDate) {
    this.toDate = toDate;
  }

  public ConversionEligibilityData newField(String newField) {
    this.newField = newField;
    return this;
  }

  /**
   * Nothing to do
   * @return newField
  */
  @ApiModelProperty(value = "Nothing to do")


  public String getNewField() {
    return newField;
  }

  public void setNewField(String newField) {
    this.newField = newField;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConversionEligibilityData conversionEligibilityData = (ConversionEligibilityData) o;
    return Objects.equals(this.fromDate, conversionEligibilityData.fromDate) &&
        Objects.equals(this.toDate, conversionEligibilityData.toDate) &&
        Objects.equals(this.newField, conversionEligibilityData.newField);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fromDate, toDate, newField);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConversionEligibilityData {\n");
    
    sb.append("    fromDate: ").append(toIndentedString(fromDate)).append("\n");
    sb.append("    toDate: ").append(toIndentedString(toDate)).append("\n");
    sb.append("    newField: ").append(toIndentedString(newField)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

