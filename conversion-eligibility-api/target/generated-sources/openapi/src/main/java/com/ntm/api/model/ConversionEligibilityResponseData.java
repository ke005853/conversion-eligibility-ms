package com.ntm.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ntm.api.model.ConversionEliDetailsInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * The response of the Conversion Eligibility 
 */
@ApiModel(description = "The response of the Conversion Eligibility ")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-07-09T10:40:26.609-05:00[America/Chicago]")

public class ConversionEligibilityResponseData   {
  @JsonProperty("fromDate")
  private LocalDate fromDate;

  @JsonProperty("toDate")
  private LocalDate toDate;

  @JsonProperty("conversionEliDetails")
  @Valid
  private List<ConversionEliDetailsInfo> conversionEliDetails = null;

  public ConversionEligibilityResponseData fromDate(LocalDate fromDate) {
    this.fromDate = fromDate;
    return this;
  }

  /**
   * From date
   * @return fromDate
  */
  @ApiModelProperty(value = "From date")

  @Valid

  public LocalDate getFromDate() {
    return fromDate;
  }

  public void setFromDate(LocalDate fromDate) {
    this.fromDate = fromDate;
  }

  public ConversionEligibilityResponseData toDate(LocalDate toDate) {
    this.toDate = toDate;
    return this;
  }

  /**
   * To date
   * @return toDate
  */
  @ApiModelProperty(value = "To date")

  @Valid

  public LocalDate getToDate() {
    return toDate;
  }

  public void setToDate(LocalDate toDate) {
    this.toDate = toDate;
  }

  public ConversionEligibilityResponseData conversionEliDetails(List<ConversionEliDetailsInfo> conversionEliDetails) {
    this.conversionEliDetails = conversionEliDetails;
    return this;
  }

  public ConversionEligibilityResponseData addConversionEliDetailsItem(ConversionEliDetailsInfo conversionEliDetailsItem) {
    if (this.conversionEliDetails == null) {
      this.conversionEliDetails = new ArrayList<>();
    }
    this.conversionEliDetails.add(conversionEliDetailsItem);
    return this;
  }

  /**
   * List of Conversations by hour 
   * @return conversionEliDetails
  */
  @ApiModelProperty(value = "List of Conversations by hour ")

  @Valid
@Size(min=0) 
  public List<ConversionEliDetailsInfo> getConversionEliDetails() {
    return conversionEliDetails;
  }

  public void setConversionEliDetails(List<ConversionEliDetailsInfo> conversionEliDetails) {
    this.conversionEliDetails = conversionEliDetails;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConversionEligibilityResponseData conversionEligibilityResponseData = (ConversionEligibilityResponseData) o;
    return Objects.equals(this.fromDate, conversionEligibilityResponseData.fromDate) &&
        Objects.equals(this.toDate, conversionEligibilityResponseData.toDate) &&
        Objects.equals(this.conversionEliDetails, conversionEligibilityResponseData.conversionEliDetails);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fromDate, toDate, conversionEliDetails);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConversionEligibilityResponseData {\n");
    
    sb.append("    fromDate: ").append(toIndentedString(fromDate)).append("\n");
    sb.append("    toDate: ").append(toIndentedString(toDate)).append("\n");
    sb.append("    conversionEliDetails: ").append(toIndentedString(conversionEliDetails)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

