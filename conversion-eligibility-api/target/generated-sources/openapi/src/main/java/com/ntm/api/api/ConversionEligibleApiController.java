package com.ntm.api.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Optional;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-07-09T10:40:26.609-05:00[America/Chicago]")

@Controller
@RequestMapping("${openapi.customerMigrationConversionEligibility.base-path:/care/v1}")
public class ConversionEligibleApiController implements ConversionEligibleApi {

    private final ConversionEligibleApiDelegate delegate;

    public ConversionEligibleApiController(@org.springframework.beans.factory.annotation.Autowired(required = false) ConversionEligibleApiDelegate delegate) {
        this.delegate = Optional.ofNullable(delegate).orElse(new ConversionEligibleApiDelegate() {});
    }

    @Override
    public ConversionEligibleApiDelegate getDelegate() {
        return delegate;
    }

}
