package com.ntm.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.time.LocalDate;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * The response of the Conversion Eligibility data info records 
 */
@ApiModel(description = "The response of the Conversion Eligibility data info records ")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2020-07-09T10:40:26.609-05:00[America/Chicago]")

public class ConversionEliDetailsInfo   {
  @JsonProperty("date")
  private LocalDate date;

  @JsonProperty("time")
  private BigDecimal time;

  @JsonProperty("count")
  private BigDecimal count;

  @JsonProperty("timeInNS")
  private BigDecimal timeInNS;

  public ConversionEliDetailsInfo date(LocalDate date) {
    this.date = date;
    return this;
  }

  /**
   * date
   * @return date
  */
  @ApiModelProperty(value = "date")

  @Valid

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public ConversionEliDetailsInfo time(BigDecimal time) {
    this.time = time;
    return this;
  }

  /**
   * time
   * @return time
  */
  @ApiModelProperty(value = "time")

  @Valid

  public BigDecimal getTime() {
    return time;
  }

  public void setTime(BigDecimal time) {
    this.time = time;
  }

  public ConversionEliDetailsInfo count(BigDecimal count) {
    this.count = count;
    return this;
  }

  /**
   * Number of Consersations or Customers
   * @return count
  */
  @ApiModelProperty(value = "Number of Consersations or Customers")

  @Valid

  public BigDecimal getCount() {
    return count;
  }

  public void setCount(BigDecimal count) {
    this.count = count;
  }

  public ConversionEliDetailsInfo timeInNS(BigDecimal timeInNS) {
    this.timeInNS = timeInNS;
    return this;
  }

  /**
   * Avg Processing time in Nano Sec
   * @return timeInNS
  */
  @ApiModelProperty(value = "Avg Processing time in Nano Sec")

  @Valid

  public BigDecimal getTimeInNS() {
    return timeInNS;
  }

  public void setTimeInNS(BigDecimal timeInNS) {
    this.timeInNS = timeInNS;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConversionEliDetailsInfo conversionEliDetailsInfo = (ConversionEliDetailsInfo) o;
    return Objects.equals(this.date, conversionEliDetailsInfo.date) &&
        Objects.equals(this.time, conversionEliDetailsInfo.time) &&
        Objects.equals(this.count, conversionEliDetailsInfo.count) &&
        Objects.equals(this.timeInNS, conversionEliDetailsInfo.timeInNS);
  }

  @Override
  public int hashCode() {
    return Objects.hash(date, time, count, timeInNS);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConversionEliDetailsInfo {\n");
    
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    time: ").append(toIndentedString(time)).append("\n");
    sb.append("    count: ").append(toIndentedString(count)).append("\n");
    sb.append("    timeInNS: ").append(toIndentedString(timeInNS)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

